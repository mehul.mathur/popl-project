module Parser where

import Datatypes
import Data.List
import Data.Maybe
import Text.Read
import Text.Parsec
import Text.Parsec.Expr
import Text.Parsec.Combinator
import Data.Functor

slice :: Int -> Int -> [a] -> [a]
slice from to xs = take (to - from + 1) (drop from xs)

splitBySemicolon     :: String -> [String]
splitBySemicolon s =  case dropWhile (==';') s of
                      "" -> []
                      s' -> w : splitBySemicolon s''
                            where (w, s'') = break (==';') s'

splitByComma     :: String -> [String]
splitByComma s =  case dropWhile (==',') s of
                      "" -> []
                      s' -> w : splitByComma s''
                            where (w, s'') = break (==',') s'

splitByVert     :: String -> [String]
splitByVert s =  case dropWhile (=='|') s of
                      "" -> []
                      s' -> w : splitByVert s''
                            where (w, s'') = break (=='|') s'

parser :: String -> Prog -- converts string to space separated tokens and then parses them 
parser s = Prog (map mainParser (map words (splitBySemicolon s)))


-- applies appropriate processing according to the keyword
mainParser :: [String] -> AST
mainParser comm@(keyword:rest) = case keyword of 
    "print" -> Command (printParser comm)
    "if" -> Command (ifParser comm)
    'p':'l':'a':'y':_ -> Command (playParser comm)
    "sleep" -> Command (sleepParser comm)
    "call" -> Command (funcCallParser comm)
    "def" -> Command (funcDefParser comm)
    "ret" -> Command (retParser comm)
    "None" -> None
    _ -> (case (length rest) of
        0 -> Expr (aexpParser comm)
        _ ->  (case (head rest) of
            "=" -> Command (assgnParser comm)
            _ -> Expr (aexpParser comm)))
        

mainParser [] = None

-- command parsers

-- print and sleep
printParser :: [String] -> Comm
printParser ("print":printable) = PrintComm (aexpParser printable)

sleepParser :: [String] -> Comm
sleepParser ("sleep":[sleepTime]) = SleepComm (numParser sleepTime)

--if parser

getThen :: [String] -> [String]
getThen s = case (head s) of
    "then" -> tail s
    _ -> getThen (tail s)

getElse :: [String] -> [String]
getElse s = case (head s) of
    "else" -> tail s
    _ -> getElse (tail s)

beforeElse :: [String] -> [String]
beforeElse s = case (last s) of
    "else" -> init s
    _ -> beforeElse (init s)


ifParser :: [String] -> Comm
ifParser ("if":rest) = (let thenIdx = fromJust (elemIndex "then" rest)
                            elseIdx = fromJust (elemIndex "else" rest)
                            condition = (getToParams (slice 0 thenIdx rest))
                            thenstmt = (slice (thenIdx+1) (elseIdx-1) rest)
                            elsestmt = (slice (elseIdx+1) ((length rest) - 1) rest)
                        in (case (aexpParser condition) of 
                            Aexpr cond -> Ifdef cond (mainParser thenstmt) (mainParser elsestmt)
                            _ -> error "Invalid if/ Not a condition"))

-- play and playTarai
playParser :: [String] -> Comm
playParser ("play":playArgs) = RunComm (Play (map varNumParser (tail playArgs)) (beatParser (head playArgs)))
playParser ("playTarai":taraiArgs) = RunComm (PlayTarai (varNumParser (head $ tail $ taraiArgs)) (varNumParser (head $ tail $ tail $ taraiArgs)) (varNumParser (head $ tail $ tail $ tail $ taraiArgs)) (beatParser (head taraiArgs)))

--assignment
assgnParser :: [String] -> Comm
assgnParser (varName:"=":assignedVal) = Assign (Bind varName (mainParser assignedVal))


-- aexp parsers
numParser :: String -> Int
numParser s = case readMaybe s::(Maybe Int) of
    Just i -> i
    Nothing -> error "Not a number/float"

varNumParser :: String -> Aexp
varNumParser s = case readMaybe s::(Maybe Int) of
    Just i -> Num i
    Nothing -> Variable s


beatParser :: String -> Aexp
beatParser s = case readMaybe s::(Maybe Float) of
    Just b -> Dec b
    Nothing -> Variable s

-- Arithmetic Expression parsing code
expr = buildExpressionParser table factor <?> "expr"

table = [[op "*" (Mul) AssocRight]
        ,[op "+" (Plus) AssocLeft, op "-" (Minus) AssocLeft]
        ,[op "<" (Less) AssocLeft , op ">" (Great) AssocLeft , op "==" (Equal) AssocLeft]]
        where op s f assoc = Infix (f <$ string s) assoc
              prefix s f = Prefix (f <$ string s)

factor = between (char '(') (char ')') expr
         <|> (Num . read <$> many1 digit <|> Variable . read . show <$> many1 alphaNum) <?> "factor"

aexpParser :: [String] -> Exp
-- aexpParser a = case (parse expr "" (intercalate "" a)) of
--     Right ret' -> Aexpr ret'
--     Left err -> (case (length a) of
--         1 -> nonKeywordParser (intercalate "" a)
--         _ -> error "Too many arguments")

aexpParser a = (case (elemIndex '.' (intercalate "" a)) of
    Just i -> Aexpr (Dec (read (intercalate "" a)::(Float)))
    Nothing -> case (parse expr "" (intercalate "" a)) of
        Right ret' -> Aexpr ret'
        Left err -> (case (length a) of
            1 -> nonKeywordParser (intercalate "" a)
            _ -> error "Too many arguments"))

nonKeywordParser :: String -> Exp
nonKeywordParser s = case (head s) of
    '\"' -> case (last s) of
        '\"' -> Str (init (tail s))
        _ -> error "Missing quotes"
    _ -> Aexpr (Variable s)


-- function calling
funcCallParser :: [String] -> Comm
funcCallParser ("call":func_name:"(":args) = case (last args) of
    ")" -> Call func_name (paramParser (init args))
    _ -> error "Invalid syntax - Missing closing brackets"

funcCallParser ("call":[func_name]) = Call func_name []


-- function definition
getToFuncBody :: [String] -> [String]
getToFuncBody s = case (head s) of
    ")" -> tail s
    _ -> getToFuncBody (tail s)

afterOpening :: [String] -> [String]
afterOpening s = case (head s) of
    "(" -> tail s
    _ -> afterOpening (tail s)

beforeClosing :: [String] -> [String]
beforeClosing s = case (last s) of
    ")" -> init s
    _ -> beforeClosing (init s)

getToParams :: [String] -> [String]
getToParams = afterOpening . beforeClosing

bodyParser :: [String] -> Prog
bodyParser s = Prog (map mainParser (map words (splitByVert (intercalate " " s))))

paramParser :: [String] -> [AST]
paramParser s = (map mainParser (map words (splitByComma (intercalate " " s))))


funcDefParser :: [String] -> Comm
funcDefParser ("def":func_name:rest) = case (last rest) of
    "end" -> (let colIndex = fromJust (elemIndex ":" (init rest))
                  params = (getToParams (slice 0 colIndex (init rest)))
                  body = slice (colIndex+1) ((length (init rest))-1) (init rest)
              in FuncDef {func_id = func_name , formals = (paramParser params) , body = (bodyParser body)})
    _ -> error "Invalid function definition / Missing \'end\'"

-- Return command
retParser :: [String] -> Comm
retParser ("ret":ret_exp) = Ret (aexpParser ret_exp)