module MusicGeneration where

import Datatypes
import qualified Data.ByteString.Lazy as B1
import qualified Data.ByteString.Builder as B
import Data.Foldable
import System.Process
import Text.Printf
import Data.List

import Prelude hiding (catch)
import System.Directory
import Control.Exception
import System.IO.Error hiding (catch)

--- Utility Functions ---

volume :: Float
volume = 0.2

sampleRate :: Samples
sampleRate = 48000.0

pitchStandard :: Hz
pitchStandard = 440.0

bpm :: Beat
bpm = 120.0

beatDuration :: Seconds
beatDuration = 60.0 / bpm

f :: Note -> Hz
f n = pitchStandard * ((2 ** (1.0 / 12.0)) ** (fromIntegral n))

note :: Beat -> Note -> [Pulse]
note beat n = freq (f n) (beat * beatDuration)


freq :: Hz -> Seconds -> [Pulse]
freq hz duration =
  map (* volume) $ zipWith3 (\x y z -> x * y * z) release attack output
  where
    step = (hz * 2 * pi) / sampleRate

    attack :: [Pulse]
    attack = map (min 1.0) [0.0,0.001 ..]

    release :: [Pulse]
    release = reverse $ take (length output) attack

    output :: [Pulse]
    output = map sin $ map (* step) [0.0 .. sampleRate * duration]


--- File Utility Functions ---

outputFilePath :: FilePath
outputFilePath = "output.bin"


save :: FilePath -> [Pulse] -> IO ()
save filePath wave = B1.appendFile filePath $ B.toLazyByteString $ fold $ map B.floatLE wave


removeIfExists :: FilePath -> IO ()
removeIfExists fileName = removeFile fileName `catch` handleExists
  where handleExists e
          | isDoesNotExistError e = return ()
          | otherwise = throwIO e

createFile :: FilePath -> IO()
createFile fileName = writeFile fileName "" 