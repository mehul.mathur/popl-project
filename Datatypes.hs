module Datatypes where

import System.Process
import Data.Map (Map)
import qualified Data.Map as Map
import Control.Monad.Except
import Data.IORef
import Data.Char

-- type recastings
type Note =  Int
type Id = String
type Error = String
type Beat = Float


--Utility Datatypes

type Pulse = Float
type Seconds = Float
type Samples = Float
type Hz = Float

-- custom data types
data Prog = Prog [AST] deriving (Show)
data AST = Command Comm | Expr Exp | None deriving (Show) 

  -- value types
data Exp = Str String | Aexpr Aexp deriving (Show)
data Aexp = Num Int | Dec Float | Boolean Bool | Variable Id | Plus Aexp Aexp | Minus Aexp Aexp | Mul Aexp Aexp | Less Aexp Aexp | Great Aexp Aexp | Equal Aexp Aexp deriving (Show)

  -- command types
data Comm = RunComm PlayExp | Assign Binding | Ifdef Aexp AST AST | FuncDef {func_id :: Id , formals :: [AST] , body :: Prog}
 | Call Id [AST] | Ret Exp | SleepComm Int | PrintComm Exp deriving (Show) 
data PlayExp = Play [Aexp] Aexp | PlayTarai Aexp Aexp Aexp Aexp deriving (Show)
data Binding = Bind Id AST deriving (Show)

-- Env
-- type Env = Map Id Exp
data Env = Env { mapping:: Map Id Exp , parent :: Env} | Empty deriving (Show)

