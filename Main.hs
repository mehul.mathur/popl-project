import Datatypes
import Parser
import qualified Data.Map as Map    
import System.Environment
import Data.Foldable
import System.Process
import Text.Printf
import Data.List
import MusicGeneration
import Interpreter

main = do
  -- removeIfExists outputFilePath 
  createFile outputFilePath
  args <- getArgs
  content <- readFile (args !! 0)
  let prog = parser content
  putStrLn ("Parsing Done :)")
  putStrLn(show prog)
  putStrLn("... Generating Music ...")
  mainInterpreter prog 
  _ <- runCommand $ printf "ffplay -autoexit -showmode 1 -f f32le -ar %f %s" sampleRate outputFilePath
  return ()


