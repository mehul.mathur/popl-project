module AexpParser where

import Datatypes
import Text.Parsec
import Text.Parsec.Expr
import Text.Parsec.Combinator
import Data.Functor

expr = buildExpressionParser table factor <?> "expr"


table = [[op "*" (Mul) AssocRight]
        ,[op "+" (Plus) AssocLeft, op "-" (Minus) AssocLeft]]
        where op s f assoc = Infix (f <$ string s) assoc
              prefix s f = Prefix (f <$ string s)


factor = between (char '(') (char ')') expr
         <|> (Num . read <$> many1 digit) <?> "factor"

aexpParser :: String -> Aexp
aexpParser a = case (parse expr "" a) of
    Right ret' -> ret'